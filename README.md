[<img src="https://www.ensicaen.fr/wp-content/uploads/2017/02/LogoEnsicaen.gif" width="256" >](https://www.ensicaen.fr)

# Projet Regate
## Mode d'emploi

Une fois sur l'écran de jeu, appuyez sur la barre d'espace pour démarrer une partie.
Utilisez Q et D pour vous déplacer respectivement vers la gauche et vers la droite.
Appuyez sur P pour mettre le jeu en pause.

Bon jeu !