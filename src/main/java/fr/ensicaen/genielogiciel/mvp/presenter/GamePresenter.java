package fr.ensicaen.genielogiciel.mvp.presenter;

import fr.ensicaen.genielogiciel.mvp.model.Obstacles;
import fr.ensicaen.genielogiciel.mvp.model.RaceManager;
import fr.ensicaen.genielogiciel.mvp.model.WindFromUrl;
import fr.ensicaen.genielogiciel.mvp.model.command.Command;
import fr.ensicaen.genielogiciel.mvp.model.command.ConcreteCommandLeft;
import fr.ensicaen.genielogiciel.mvp.model.command.ConcreteCommandRight;
import fr.ensicaen.genielogiciel.mvp.model.command.ConcreteCommandStraight;
import fr.ensicaen.genielogiciel.mvp.model.decorator.Boat;
import fr.ensicaen.genielogiciel.mvp.view.GameView;
import fr.ensicaen.genielogiciel.mvp.view.MenuView;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.io.IOException;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public final class GamePresenter {
    private static Rectangle _boatHitbox;
    private GameView _view;
    private final Obstacles _obstacles;
    private static final HashMap<KeyCode, Boolean> _activeKeys = new HashMap<>();
    private final RaceManager _raceManager;
    private double _initialX;
    private double _initialY;
    private Timeline _timeline;
    private Queue<Command> _commandQueue;

    public GamePresenter( int nbCrewMates, String sailSize, String collisionMap ) throws IOException {
        _raceManager = new RaceManager(nbCrewMates, sailSize);
        _obstacles = new Obstacles("src/main/resources/fr/ensicaen/genielogiciel/mvp/model/" + collisionMap);
        _boatHitbox = new Rectangle(30, 30, Color.RED);
        _obstacles.setBoat(_boatHitbox);
    }

    public Obstacles getObstacles() {
        return _obstacles;
    }

    public Rectangle getBoatHitbox() {
        return _boatHitbox;
    }

    public static HashMap<KeyCode, Boolean> get_activeKeys() {
        return _activeKeys;
    }

    public void setView( GameView view ) {
        _view = view;
    }

    public GameView getView() {
        return _view;
    }

    public void initializeGame() throws IOException {
        if (!_raceManager.is_replay()) {
            _commandQueue = new LinkedList<>();
        }
        WindFromUrl parser = new WindFromUrl();
        parser.setWindSpeedAndDirection();
        float windAngle = parser.getWindAngle();
        _view.setArrowAngle((windAngle + 180) % 360);
        Line startingLine = _obstacles.getStartingLine();
        _initialX = (startingLine.getStartX() + startingLine.getEndX()) / 2;
        _initialY = (startingLine.getStartY() + startingLine.getEndY()) / 2;
        _raceManager.getBoat().setAngle(_obstacles.getCheckpointAngle());
        _raceManager.getBoat().setX(_initialX);
        _raceManager.getBoat().setY(_initialY);
        _raceManager.getBoat().activate();
        _view.setArrowAngle((_raceManager.get_windAngle()+180)%360);
    }

    public void moveToLastCheckPoint( Boat b ) {
        Line lastCheckPoint = _obstacles.getLastCheckPoint();
        int newX = (int) (lastCheckPoint.getStartX() + lastCheckPoint.getEndX()) / 2 - 60;
        int newY = (int) (lastCheckPoint.getStartY() + lastCheckPoint.getEndY()) / 2 - 60;
        b.setAngle(_obstacles.getCheckpointAngle());
        b.setX(newX);
        b.setY(newY);
        _view.moveBoat(_obstacles.getCheckpointAngle(), newX, newY);
    }

    public Timeline getTimeline() {
        return _timeline;
    }

    public void runGameLoop() throws IOException {

        if (!_raceManager.is_replay()) {
            initializeGame();
        }

        final int FRAME_PER_SECONDS = 20;
        _raceManager.setPaused(false);
        _raceManager.setGameOver(false);
        _timeline = new Timeline(new KeyFrame(Duration.millis(FRAME_PER_SECONDS), onFinished -> {
            if (!_raceManager.isGameOver()) {
                pauseGame();
                if (!_raceManager.isPaused()) {
                    update();
                    render();
                }
            } else {
                _view.endGame(_raceManager.getDuration());
            }
        }));
        _timeline.setCycleCount(Animation.INDEFINITE);
        _timeline.play();
    }

    public void launchMenu() throws IOException {
        MenuView view = MenuView.createView();
        MenuPresenter menuPresenter = new MenuPresenter();
        view.setPresenter(menuPresenter);
        menuPresenter.setView(view);
        view.show();
    }

    public void resumeGame() {
        _raceManager.setPaused(false);
    }

    public void playAgain() throws IOException {
        _commandQueue.poll();
        _raceManager.set_replay(true);
        _timeline.stop();
        Boat b = _raceManager.getBoat();
        b.activate();
        _view.moveBoat(_raceManager.get_startingAngle(), _initialX - 60, _initialY - 60);
        b.setX(_initialX - 60);
        b.setY(_initialY - 60);
        b.setAngle(_raceManager.get_startingAngle());
        runGameLoop();
    }

    private void update() {
        Boat b = _raceManager.getBoat();
        _view.getSpeedGauge().setValue(b.getSpeed() * 4);
        Command command;
        if (_raceManager.is_replay()) {
            command = _commandQueue.poll();
        } else {
            command = userAction();
            _commandQueue.add(command);
        }
        _obstacles.checkNewCheckPoint();
        _obstacles.lapFinished();
        if (_obstacles.checkCollision()) {
            moveToLastCheckPoint(b);
        } else {
            assert command != null;
            b.updatePosition(command.execute(), _raceManager.getWp());
            _view.moveBoat(b.getAngle(), b.getX(), b.getY());
        }

        if (_obstacles.hasArrived()) {
            b.neutralize();
            _raceManager.setGameOver(true);
        }
    }

    private void render() {
        _raceManager.setDuration(java.time.Duration.between(_raceManager.get_startTime(), LocalTime.now()));
        Boat b = _raceManager.getBoat();
        _view.getSpeedGauge().setValue(b.getSpeed()*4);
        _view.getCompassGauge().setValue(b.getAngle());
        _view.updateDashboard(_raceManager.getDuration().getSeconds(), _raceManager.get_windSpeed());
        if (_commandQueue.isEmpty()) {
            _raceManager.setGameOver(true);
            _raceManager.set_replay(false);
            try {
                _view.launchMenu();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        if (_obstacles.getCheckpointsCrossed().size() != 0) {
            _view.setValidCheckpoint(_obstacles.getCheckpointsCrossed().size() - 1);
        }
        if(_obstacles.getCheckpointsValidated()) {
            _view.activateFinishLine();
        } else {
            _view.disableFinishLine();
        }
        if (_obstacles.getCheckpointsCrossed().size() == _obstacles.getCheckpoints().size()) {
            _view.resetLastCheckpointsActivated();
        }
        _view.getCompassGauge().setValue(b.getAngle());
    }

    private void pauseGame() {
        if (_activeKeys.containsKey(KeyCode.X)) {
            _raceManager.setGameOver(true);
        }
        if (_activeKeys.containsKey(KeyCode.P)) {
            _raceManager.setPaused(true);
            _view._pauseView.setDisable(false);
            _view._pauseView.setOpacity(100);
        }
    }

    public Command userAction() {
        if (_activeKeys.containsKey(KeyCode.Q) && _activeKeys.containsKey(KeyCode.D)) {
            return new ConcreteCommandStraight();
        }
        if (_activeKeys.containsKey(KeyCode.Q)) {
            return new ConcreteCommandLeft();
        }
        if (_activeKeys.containsKey(KeyCode.D)) {
            return new ConcreteCommandRight();
        }
        return new ConcreteCommandStraight();
    }
}
