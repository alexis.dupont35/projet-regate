package fr.ensicaen.genielogiciel.mvp.presenter;

import fr.ensicaen.genielogiciel.mvp.LoginMain;
import fr.ensicaen.genielogiciel.mvp.view.GameView;
import fr.ensicaen.genielogiciel.mvp.view.MenuView;

import java.io.IOException;

public class MenuPresenter {
    private MenuView _view;

    public MenuView getView() { return _view; }

    public void launchGame( String nickName ) {
        if (nickName.isEmpty()) {
            _view.displayError(LoginMain.getMessageBundle().getString("error.nickname"));
        } else {
            try {
                createAndDisplayGameView(_view.getTeamMates(), _view.getSailSize(), _view.getImage(), getCollisionBoxes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            _view.close();
        }
    }



    public String getCollisionBoxes() {
        if (_view.getListMap().get(_view.getNumberMap()).equals(MenuView.class.getResource("map_easy.png").toString())) {
            return "Map1.txt";
        } else {
            return "Map2.txt";
        }
    }

    public void createAndDisplayGameView(int teamMates, String sailSize, String image, String collisionMap) throws IOException {
        System.out.println(image);
        GamePresenter gamePresenter = new GamePresenter(teamMates, sailSize, collisionMap);
        GameView view = GameView.createView(image);
        view.setPresenter(gamePresenter);
        view.initializeGameView();
        gamePresenter.setView(view);
        view.show();
    }

    public void setView(MenuView view) { _view = view; }
}
