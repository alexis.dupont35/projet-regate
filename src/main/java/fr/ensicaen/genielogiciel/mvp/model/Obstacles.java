package fr.ensicaen.genielogiciel.mvp.model;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Obstacles {


    private final ArrayList<Line> checkpointsCrossed;
    private final ArrayList<Integer> checkpointAngles;
    private ArrayList<Rectangle> collisionHitboxes;
    private ArrayList<Line> checkpoints;
    private Line startingLine;
    private int startingAngle;
    private Line finishLine;
    private Rectangle boat;
    private Boolean checkpointsValidated = false;
    private Line lastCheckPoint;
    private int checkpointAngle;
    private int _nbLap;

    public Obstacles(String mapName) {
        checkpointAngles = new ArrayList<>();
        checkpointsCrossed = new ArrayList<>();
        try {
            retrieveHitboxes(mapName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        lastCheckPoint = startingLine;
    }

    public void setLastCheckPoint( Line lastCheckPoint ) {
        this.lastCheckPoint = lastCheckPoint;
    }

    public Line getLastCheckPoint() {
        return lastCheckPoint;
    }

    public void retrieveHitboxes( String mapName ) throws FileNotFoundException {
        String[] line;
        int nbObstacles;
        int nbBeacons;
        Scanner sc = new Scanner((new File(mapName)));
        line = sc.nextLine().split(" ");
        nbObstacles = Integer.parseInt(line[0]);
        nbBeacons = Integer.parseInt(line[1]);
        collisionHitboxes = retrieveCollisionHitbox(nbObstacles, sc);
        checkpoints = retrieveBeacons(nbBeacons, sc);
        startingLine = retrieveStartingLine(sc);
    }

    public Line retrieveStartingLine(Scanner sc) {
        String[] line = sc.nextLine().split(" ");
        Line l = new Line();
        l.setStartX(Integer.parseInt(line[0]));
        l.setStartY(Integer.parseInt(line[1]));
        l.setEndX(Integer.parseInt(line[2]));
        l.setEndY(Integer.parseInt(line[3]));
        l.setStroke(Color.RED);
        collisionHitboxes.add(createHitbox(Integer.parseInt(line[0]) - 13, Integer.parseInt(line[1]) - 6,
                                25, 30));
        collisionHitboxes.add(createHitbox(Integer.parseInt(line[2]) - 13, Integer.parseInt(line[3]) - 6,
                                25, 30));
        checkpointAngle = Integer.parseInt(line[4]);
        startingAngle = checkpointAngle;
        return l;
    }

    public ArrayList<Rectangle> retrieveCollisionHitbox(int nbObstacles, Scanner sc) {
        Rectangle r;
        ArrayList<Rectangle> rectangles = new ArrayList<>();
        String[] line;
        for (int i = 0; i < nbObstacles; i++) {
            line = sc.nextLine().split(" ");
            r = createHitbox(Integer.parseInt(line[0]), Integer.parseInt(line[1]),
                                Integer.parseInt(line[2]), Integer.parseInt(line[3]));
            rectangles.add(r);
        }
        return rectangles;
    }


    public ArrayList<Line> retrieveBeacons( int nbBeacons, Scanner sc ) {
        Line l;
        ArrayList<Line> beacons = new ArrayList<>();
        String[] line;
        Rectangle beaconHitbox;
        for (int i = 0; i < nbBeacons; i++) {
            line = sc.nextLine().split(" ");
            l = new Line();
            l.setStartX(Integer.parseInt(line[0]));
            l.setStartY(Integer.parseInt(line[1]));
            collisionHitboxes.add(createHitbox(Integer.parseInt(line[0]) - 13, Integer.parseInt(line[1]) - 6,
                                                    25, 30));
            l.setEndX(Integer.parseInt(line[2]));
            l.setEndY(Integer.parseInt(line[3]));
            collisionHitboxes.add(createHitbox(Integer.parseInt(line[2]) - 13, Integer.parseInt(line[3]) - 6,
                                                    25, 30));
            l.setStroke(Color.YELLOW);
            beacons.add(l);
            checkpointAngles.add(Integer.parseInt(line[4]));
        }
        return beacons;
    }


    public boolean checkCollision() {
        for (Rectangle r : collisionHitboxes) {
            if (boat.getBoundsInParent().intersects(r.getBoundsInParent())) {
                return true;
            }
        }
        return false;
    }

    public void checkpointValidation(Line checkpoint) {
        if (checkpointsCrossed.size() == checkpoints.size()) {
            checkpointsValidated = true;
        }
        if (!checkpointsCrossed.contains(checkpoint)) {
            if (checkpoints.get(checkpointsCrossed.size()).equals(checkpoint)) {
                checkpointsCrossed.add(checkpoint);
                lastCheckPoint = checkpoint;
                checkpointAngle = checkpointAngles.get(checkpointsCrossed.size()-1);
            }
        }
    }

    public void checkNewCheckPoint() {
        for (Line l : checkpoints) {
            if (boat.getBoundsInParent().intersects(l.getBoundsInParent())) {
                checkpointValidation(l);
            }
        }
    }

    public void lapFinished() {
        if (checkpointsValidated && boat.getBoundsInParent().intersects(startingLine.getBoundsInParent())) {
            _nbLap++;
            lastCheckPoint = startingLine;
            checkpointAngle = startingAngle;
            if (_nbLap != 3) {
                checkpointsValidated = false;
            }
            checkpointsCrossed.clear();
        }
    }

    public Boolean hasArrived() {
        if (checkpointsValidated && _nbLap == 3) {
            checkpointsCrossed.clear();
            lastCheckPoint = startingLine;
            checkpointAngle = startingAngle;
            checkpointsValidated = false;
            _nbLap = 0;
            return true;
        } else {
            return false;
        }
    }
    public Rectangle createHitbox(int x, int y, int width, int height) {
        Rectangle r;
        r = new Rectangle(width, height, Color.GREEN);
        r.setX(x);
        r.setY(y);
        return r;
    }

    public Rectangle getBoat() {
        return boat;
    }

    public void setBoat(Rectangle b) {
        this.boat = b;
    }

    public ArrayList<Rectangle> getCollisionHitboxes() {
        return collisionHitboxes;
    }

    public ArrayList<Line> getCheckpoints() { return checkpoints; }

    public Line getStartingLine() {
        return startingLine;
    }

    public Boolean getCheckpointsValidated() {
        return checkpointsValidated;
    }

    public int getCheckpointAngle() {
        return checkpointAngle;
    }

    public ArrayList<Line> getCheckpointsCrossed() {
        return checkpointsCrossed;
    }
}
