package fr.ensicaen.genielogiciel.mvp.model;

import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class WindFromUrl {
    private float windSpeed;
    private String windDirection;

    public void setWindSpeedAndDirection() throws IOException {
        JSONObject json = readJsonFromUrl("https://www.prevision-meteo.ch/services/json/lat=49.283lng=-0.25");
        this.windSpeed = json.getJSONObject("current_condition").getFloat("wnd_spd");
        this.windDirection = json.getJSONObject("current_condition").getString("wnd_dir");
    }

    private static String readAll( Reader rd ) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public JSONObject readJsonFromUrl( String url ) throws IOException {
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
        }
    }


    public float getWindSpeed() {

        return windSpeed;
    }

    public String getWindDirection() {

        return windDirection;
    }

    public float getWindAngle() {

        switch (windDirection) {
            case "NNE":
                return (float) 22.5;
            case "NE":
                return 45;
            case "ENE":
                return (float) 67.5;
            case "E":
                return 90;
            case "ESE":
                return (float) 112.5;
            case "SE":
                return 135;
            case "SSE":
                return (float) 157.5;
            case "S":
                return 180;
            case "SSO":
                return (float) 202.5;
            case "SO":
                return 225;
            case "OSO":
                return (float) 247.5;
            case "O":
                return 270;
            case "ONO":
                return (float) 292.5;
            case "NO":
                return 315;
            case "NNO":
                return (float) 337.5;
            default:
                return 0;
        }
    }
}
