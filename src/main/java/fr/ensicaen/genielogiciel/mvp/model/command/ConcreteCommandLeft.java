package fr.ensicaen.genielogiciel.mvp.model.command;

public class ConcreteCommandLeft extends Command {
    @Override
    public int execute() {
        return -1;
    }
}
