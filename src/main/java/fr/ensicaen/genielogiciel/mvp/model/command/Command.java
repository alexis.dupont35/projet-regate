package fr.ensicaen.genielogiciel.mvp.model.command;

public abstract class Command {
    public abstract int execute();
}
