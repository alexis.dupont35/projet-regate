package fr.ensicaen.genielogiciel.mvp.model.decorator;

import fr.ensicaen.genielogiciel.mvp.model.WindParser;

public class MediumSailedBoatDecorator extends BoatDecorator {

    public MediumSailedBoatDecorator(Boat boat) {
        super(boat);
    }

    @Override
    public double getNewAngle(int userAction, double currentAngle) {
        return super.getNewAngle(userAction, currentAngle);
    }

    @Override
    public double getNewSpeed(WindParser wp) {
        return calculateSpeed(wp, 19, 14) / 4;
    }

    public void setAngle(double checkpointAngle) {
        super.setAngle(checkpointAngle);
    }
}
