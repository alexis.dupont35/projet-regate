package fr.ensicaen.genielogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.decorator.*;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;


public class RaceManager {

    private final WindParser wp;
    private final float _windAngle;
    private final float _windSpeed;
    private final LocalTime _startTime;
    private boolean _isGameOver;
    private boolean _replay;
    private boolean _isPaused;
    private double _startingAngle;
    private java.time.Duration _duration;


    private Boat boat;

    public WindParser getWp() {
        return wp;
    }

    public Boat getBoat() {
        return boat;
    }

    public RaceManager(int nbCrewMates, String sailSize) throws IOException {
        _isGameOver = false;
        _startTime = LocalTime.now();
        WindFromUrl parser = new WindFromUrl();
        parser.setWindSpeedAndDirection();
        float windSpeed = parser.getWindSpeed();
        float windAngle = parser.getWindAngle();
        _windAngle = windAngle;
        _windSpeed = windSpeed;
        wp = new WindParser(windSpeed, windAngle);
        boat = new SimpleBoat();
        if (nbCrewMates == 2) {
            boat = new TwoCrewMatesBoatDecorator(boat);
        } else {
            boat = new FourCrewMatesBoatDecorator(boat);
        }
        if (sailSize.equals("big")) {
            boat = new BigSailedBoatDecorator(boat);
        } else {
            boat = new MediumSailedBoatDecorator(boat);
        }
    }

    public float get_windAngle() {
        return _windAngle;
    }

    public float get_windSpeed() {
        return _windSpeed;
    }

    public boolean isGameOver() {
        return _isGameOver;
    }

    public boolean is_replay() {
        return _replay;
    }

    public boolean isPaused() {
        return _isPaused;
    }

    public void setGameOver(boolean _isGameOver) {
        this._isGameOver = _isGameOver;
    }

    public void set_replay(boolean _replay) {
        this._replay = _replay;
    }

    public void setPaused(boolean _isPaused) {
        this._isPaused = _isPaused;
    }

    public LocalTime get_startTime() {
        return _startTime;
    }

    public double get_startingAngle() { return _startingAngle; }

    public Duration getDuration() {
        return _duration;
    }

    public void setDuration( Duration duration ) {
        _duration = duration;
    }
}
