package fr.ensicaen.genielogiciel.mvp.model.command;

public class ConcreteCommandRight extends Command {
    @Override
    public int execute() {
        return 1;
    }
}
