package fr.ensicaen.genielogiciel.mvp.model;

import java.io.File;
import java.util.Scanner;

public class WindParser {
    private final float value;
    private final float angle;
    private final double[][] speedPolar;

    public WindParser(float value, float angle) {
        this.value = value;
        this.angle = angle;
        this.speedPolar = initializeSpeedPolar();
    }

    public float getValue() {
        return value;
    }

    public float getAngle() {
        return angle;
    }

    public double[][] initializeSpeedPolar() {
        double[][] speedPolar = new double[20][15];
        int row;
        int col;
        try {
            File file = new File("src/main/resources/fr/ensicaen/genielogiciel/mvp/model/polaireFigaro.txt");
            Scanner sc = new Scanner(file);
            String line;
            speedPolar[0][0] = -1;
            if (sc.hasNextLine()) {
                line = sc.nextLine();
                col = 1;
                for (String s : line.split("\\t")) {
                    speedPolar[0][col] = Double.parseDouble(s);
                    col++;
                }
            }
            row = 1;
            while (sc.hasNextLine()) {
                line = sc.nextLine();
                col = 0;
                for (String s : line.split("\\t")) {
                    speedPolar[row][col] = Double.parseDouble(s);
                    col++;
                }
                row++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return speedPolar;
    }

    public double[][] getSpeedPolar() {
        return speedPolar;
    }
}
