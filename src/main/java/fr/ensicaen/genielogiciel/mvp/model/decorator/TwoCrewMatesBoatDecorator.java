package fr.ensicaen.genielogiciel.mvp.model.decorator;

import fr.ensicaen.genielogiciel.mvp.model.WindParser;

public class TwoCrewMatesBoatDecorator extends BoatDecorator {
    public TwoCrewMatesBoatDecorator(Boat boat) {
        super(boat);
    }

    public double getNewAngle(int userAction, double currentAngle) {
        double angle;
        angle = (currentAngle + 1.5 * userAction) % 360;
        if (currentAngle < 0) {
            angle = currentAngle + 360;
        }
        return angle;
    }

    @Override
    public double getNewSpeed(WindParser wp) {
        return super.getNewSpeed(wp);
    }
}
