package fr.ensicaen.genielogiciel.mvp.model.decorator;

import fr.ensicaen.genielogiciel.mvp.model.WindParser;

public class SimpleBoat implements Boat {


    @Override
    public double getNewAngle(int userAction, double currentAngle) {
        return 0;
    }

    @Override
    public double getNewSpeed(WindParser wp) {
        return 0;
    }

    @Override
    public void setAngle(double checkpointAngle) {
    }

    @Override
    public void setX(double initialX) {
    }

    @Override
    public void setY(double initialY) {
    }

    @Override
    public void activate() {
    }

    @Override
    public double getAngle() { return 0; }

    @Override
    public double getX() {
        return 0;
    }

    @Override
    public double getY() {
        return 0;
    }

    @Override
    public void neutralize() {
    }

    public boolean isNeutralized() {
        return false;
    }

    @Override
    public double getSpeed() {
        return 0;
    }

    @Override
    public void updatePosition(int execute, WindParser wp) {
    }
}
