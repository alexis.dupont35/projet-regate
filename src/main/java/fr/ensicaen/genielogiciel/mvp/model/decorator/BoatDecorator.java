package fr.ensicaen.genielogiciel.mvp.model.decorator;

import fr.ensicaen.genielogiciel.mvp.model.WindParser;

import static java.lang.Math.*;

abstract class BoatDecorator implements Boat {

    private final Boat decoratedBoat;
    private double angle;
    private double x = 0;
    private double y = 0;
    private boolean neutralized = false;
    private double speed = 0;

    public BoatDecorator(Boat boat) {
        this.decoratedBoat = boat;
    }

    @Override
    public double getNewAngle(int userAction, double currentAngle) {
        return decoratedBoat.getNewAngle(userAction, currentAngle);
    }

    @Override
    public double getNewSpeed(WindParser wp) {
        return decoratedBoat.getNewSpeed(wp);
    }

    public double getRelativeAngle( WindParser wind ) {
        double angleRelative;
        angleRelative = abs(this.angle - wind.getAngle());
        if (angleRelative > 180) {
            angleRelative = 360 - angleRelative;
        }
        return angleRelative;
    }

    public double calculateSpeed(WindParser wind, int row, int col ) {
        int i;
        int j;
        i = 0;
        j = 0;

        while(( wind.getSpeedPolar()[i][0] < this.getRelativeAngle(wind) ) && ( i < row )) {
            i++;
        }
        while(( wind.getSpeedPolar()[0][j] < wind.getValue() ) && ( j < col )) {
            j++;
        }
        speed = wind.getSpeedPolar()[i][j];
        return wind.getSpeedPolar()[i][j];
    }

    public void updatePosition(int userAction, WindParser wind) {
        if (!isNeutralized()) {
            this.angle = getNewAngle(userAction, getAngle());
            this.speed = getNewSpeed(wind);
            this.x += speed * cos(this.angle * Math.PI / 180);
            this.y += speed * sin(this.angle * Math.PI / 180);
        }
    }

    public boolean isNeutralized() {
        return neutralized;
    }

    public void neutralize() {
        neutralized = true;
    }

    public void activate() {
        neutralized = false;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getSpeed() {
        return speed;
    }

    public void setAngle(double angle) { this.angle = angle; }

    public double getAngle() {
        return angle;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

}
