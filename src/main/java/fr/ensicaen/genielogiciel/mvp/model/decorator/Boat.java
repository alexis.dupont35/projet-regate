package fr.ensicaen.genielogiciel.mvp.model.decorator;

import fr.ensicaen.genielogiciel.mvp.model.WindParser;

public interface Boat {

    double getNewAngle(int userAction, double currentAngle);

    double getNewSpeed(WindParser wp);

    void activate();

    double getAngle();

    void setAngle(double checkpointAngle);

    double getX();

    void setX(double initialX);

    double getY();

    void setY(double initialY);

    void neutralize();

    boolean isNeutralized();

    double getSpeed();

    void updatePosition(int execute, WindParser wp);

}
