package fr.ensicaen.genielogiciel.mvp.model.decorator;

import fr.ensicaen.genielogiciel.mvp.model.WindParser;

public class FourCrewMatesBoatDecorator extends BoatDecorator {

    public FourCrewMatesBoatDecorator(Boat boat) {
        super(boat);
    }

    @Override
    public double getNewAngle(int userAction, double currentAngle) {
        double angle;
        angle = (currentAngle + userAction) % 360;
        if (currentAngle < 0) {
            angle = currentAngle + 360;
        }
        return angle;
    }

    @Override
    public double getNewSpeed(WindParser wp) { return super.getNewSpeed(wp); }

}
