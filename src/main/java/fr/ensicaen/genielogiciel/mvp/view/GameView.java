package fr.ensicaen.genielogiciel.mvp.view;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.TickLabelOrientation;
import eu.hansolo.medusa.skins.ModernSkin;
import fr.ensicaen.genielogiciel.mvp.presenter.GamePresenter;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;

public final class GameView {
    private static GamePresenter _gamePresenter;
    private static ImageView[] startingLineBeacons;
    private static ArrayList<ArrayList<ImageView>> checkpoints;
    private boolean finishLineActivated;

    private static AnchorPane _root;
    private static Stage _stage;
    public Group _pauseView;
    public ImageView _boat;
    public Group _endView;
    public ImageView _dashboard;
    public ImageView _arrow;
    public Text _finalTime;
    int lastCheckpointActivated;


    @FXML
    private Text _time;
    @FXML
    private Text _windSpeed;
    @FXML
    private Gauge speedGauge;
    @FXML
    private Gauge compassGauge;

    public static GameView createView( String image ) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(GameView.class.getResource(image));
        _root = fxmlLoader.load();
        final GameView view = fxmlLoader.getController();
        fxmlLoader.setController(view);

        return view;
    }

    public void initializeGameView() {
        startingLineBeacons = new ImageView[2];
        checkpoints = new ArrayList<>(2);
        checkpoints.add(new ArrayList<>());
        checkpoints.add(new ArrayList<>());
        finishLineActivated = false;
        setSpeedGaugeProperties();
        setCompassGaugeProperties();
        lastCheckpointActivated = 0;


        Line startingLine = _gamePresenter.getObstacles().getStartingLine();
        placeBeacon(startingLine, true, _root);


        for (Line line : _gamePresenter.getObstacles().getCheckpoints()) {
            placeBeacon(line, false, _root);
        }

        Scene scene = new Scene(_root, 1920, 1080);
        Stage stage = new Stage();

        stage.setMaximized(true);
        stage.setFullScreen(true);
        stage.setScene(scene);
        _stage = stage;
        scene.setOnKeyPressed(event -> {
            if (_gamePresenter.getTimeline() == null) {
                if (event.getCode() == KeyCode.SPACE) {
                    try {
                        _gamePresenter.runGameLoop();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (!GamePresenter.get_activeKeys().containsKey(event.getCode())) {
                GamePresenter.get_activeKeys().put(event.getCode(), true);
            }
        });
        scene.setOnKeyReleased(event ->
                GamePresenter.get_activeKeys().remove(event.getCode())
        );
    }


    public void updateDashboard( long time, float windSpeed ) {
        _time.setText(time + "s");
        _windSpeed.setText(windSpeed + "Km/h");
    }


    public void setPresenter( GamePresenter gamePresenter ) {

        _gamePresenter = gamePresenter;
    }

    public GamePresenter getPresenter() {
        return _gamePresenter;
    }

    public void show() {

        _stage.show();
    }

    public void setArrowAngle( double angle ) {
        _arrow.setRotate(angle);
    }

    public void placeBeacon( Line line, boolean isStartingLine, AnchorPane root ) {
        ImageView imageViewStart;
        ImageView imageViewEnd;
        ImageView startImage;
        ImageView endImage;
        if (isStartingLine) {
            startImage = new ImageView("/fr/ensicaen/genielogiciel/mvp/view/finishBeacon.png");
            endImage = new ImageView("/fr/ensicaen/genielogiciel/mvp/view/finishBeacon.png");
        } else {
            startImage = new ImageView("/fr/ensicaen/genielogiciel/mvp/view/mediumBeacon.png");
            endImage = new ImageView("/fr/ensicaen/genielogiciel/mvp/view/mediumBeacon.png");
            checkpoints.get(0).add(startImage);
            checkpoints.get(1).add(endImage);
        }
        imageViewStart = startImage;
        imageViewEnd = endImage;

        imageViewStart.setFitWidth(50);
        imageViewStart.setFitHeight(50);
        imageViewStart.setLayoutX(line.getStartX() - 25);
        imageViewStart.setLayoutY(line.getStartY() - 25);
        imageViewEnd.setFitWidth(50);
        imageViewEnd.setFitHeight(50);
        imageViewEnd.setLayoutX(line.getEndX() - 25);
        imageViewEnd.setLayoutY(line.getEndY() - 25);
        root.getChildren().add(imageViewEnd);
        root.getChildren().add(imageViewStart);


        if (isStartingLine) {
            startingLineBeacons[0] = imageViewStart;
            startingLineBeacons[1] = imageViewEnd;
        }
    }

    public void resetLastCheckpointsActivated() {
        lastCheckpointActivated = 0;
    }

    public void setValidCheckpoint(int index) {
        if (index == lastCheckpointActivated) {
            checkpoints.get(0).get(index).setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/startBeacon.png"));
            checkpoints.get(1).get(index).setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/startBeacon.png"));
            lastCheckpointActivated++;
        }
    }

    public void disableAllCheckpoints() {
        for (int i = 0; i < checkpoints.get(0).size(); i++) {
            checkpoints.get(0).get(i).setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/mediumBeacon.png"));
            checkpoints.get(1).get(i).setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/mediumBeacon.png"));
        }
    }

    public void resumeGame() {
        _pauseView.setDisable(true);
        _pauseView.setOpacity(0);
        _gamePresenter.resumeGame();
    }

    public void playAgain() throws IOException {
        _endView.setDisable(true);
        _endView.setOpacity(0);
        _gamePresenter.playAgain();
    }

    public void endGame( Duration duration ) {
        _endView.setOpacity(100);
        _endView.setDisable(false);
        _finalTime.setText("Temps : " + duration.getSeconds() + " s");
    }


    @FXML
    public void launchMenu() throws IOException {
        _stage.close();
        _gamePresenter.launchMenu();
    }

    public void activateFinishLine() {
        if (!finishLineActivated){
            startingLineBeacons[0].setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/startBeacon.png"));
            startingLineBeacons[1].setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/startBeacon.png"));
            finishLineActivated = true;
        }
    }

    public void disableFinishLine() {
        if (finishLineActivated) {
            startingLineBeacons[0].setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/finishBeacon.png"));
            startingLineBeacons[1].setImage(new Image("/fr/ensicaen/genielogiciel/mvp/view/finishBeacon.png"));
            finishLineActivated = false;
            disableAllCheckpoints();
        }
    }

    private void setSpeedGaugeProperties() {
        speedGauge.setSkin(new ModernSkin(speedGauge));
        speedGauge.setTitle("BOAT SPEED");
        speedGauge.setUnit("Km / h");
        speedGauge.setUnitColor(Color.WHITE);
        speedGauge.setDecimals(4);
        speedGauge.setValue(0);
        speedGauge.setAnimated(true);
        speedGauge.setValueColor(Color.WHITE);
        speedGauge.setTitleColor(Color.WHITE);
        speedGauge.setSubTitleColor(Color.WHITE);
        speedGauge.setBarColor(Color.rgb(0, 214, 215));
        speedGauge.setNeedleColor(Color.RED);
        speedGauge.setThresholdColor(Color.RED);
        speedGauge.setThreshold(12);
        speedGauge.setThresholdVisible(true);
        speedGauge.setTickLabelColor(Color.rgb(151, 151, 151));
        speedGauge.setTickMarkColor(Color.WHITE);
        speedGauge.setTickLabelOrientation(TickLabelOrientation.ORTHOGONAL);
        speedGauge.setMaxValue(20);
    }

    public void moveBoat( double rotation, double newX, double newY ) {
        _boat.setLayoutX(newX);
        _boat.setLayoutY(newY);
        _boat.setRotate(rotation);
        _gamePresenter.getBoatHitbox().setLayoutX(newX + 60);
        _gamePresenter.getBoatHitbox().setLayoutY(newY + 60);
        _gamePresenter.getBoatHitbox().setFill(Color.RED);
    }

    private void setCompassGaugeProperties() {
        compassGauge.setMinValue(0);
        compassGauge.setMaxValue(359);
        compassGauge.setStartAngle(90);
        compassGauge.setAngleRange(360);
        compassGauge.setCustomTickLabelsEnabled(true);
        compassGauge.setCustomTickLabels("N", "NE", "E", "SE", "S", "SW", "W", "NW");
        compassGauge.setCustomTickLabelFontSize(20);
        compassGauge.setMinorTickMarksVisible(false);
        compassGauge.setMediumTickMarksVisible(false);
        compassGauge.setMajorTickMarksVisible(false);
        compassGauge.setValueVisible(false);
        compassGauge.setNeedleType(Gauge.NeedleType.FAT);
        compassGauge.setNeedleShape(Gauge.NeedleShape.FLAT);
        compassGauge.setKnobType(Gauge.KnobType.FLAT);
        compassGauge.setKnobColor(Gauge.DARK_COLOR);
        compassGauge.setBorderPaint(Gauge.DARK_COLOR);
        compassGauge.setAnimated(true);
        compassGauge.setAnimationDuration(500);
        compassGauge.setNeedleBehavior(Gauge.NeedleBehavior.OPTIMIZED);
    }
    public Gauge getSpeedGauge() {
        return speedGauge;
    }

    public Gauge getCompassGauge() {
        return compassGauge;
    }

}

