package fr.ensicaen.genielogiciel.mvp.view;

import fr.ensicaen.genielogiciel.mvp.LoginMain;
import fr.ensicaen.genielogiciel.mvp.presenter.MenuPresenter;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MenuView {
    private MenuPresenter _presenter;
    private static Stage _stage;
    @FXML
    javafx.scene.control.Button left;
    @FXML
    private Label _errorLabel;
    @FXML
    javafx.scene.control.Button right;
    @FXML
    ImageView imageView;
    private int _numberMap;
    @FXML
    private RadioButton teamMates2;
    @FXML
    private RadioButton teamMates4;
    @FXML
    private ToggleGroup teamMates;
    @FXML
    private RadioButton medium;
    @FXML
    private RadioButton big;
    @FXML
    private ToggleGroup sailSize;
    private javafx.scene.image.Image[] images;
    private java.util.List<String> _listMap;

    public static MenuView createView(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(MenuView.class.getResource("Menu.fxml"), LoginMain.getMessageBundle());
        Parent root = loader.load();
        MenuView view = loader.getController();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        _stage = primaryStage;
        view._numberMap = 0;
        _stage.setMaximized(true);
        _stage.setFullScreen(true);

        return view;
    }

    public static MenuView createView() throws IOException {
        FXMLLoader loader = new FXMLLoader(MenuView.class.getResource("Menu.fxml"), LoginMain.getMessageBundle());
        Parent root = loader.load();
        MenuView view = loader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        _stage = stage;
        view._numberMap = 0;
        stage.setMaximized(true);
        stage.setFullScreen(true);

        return view;
    }


    public void show() {
        _stage.show();
    }

    public void close() {
        _stage.close();
    }

    public MenuPresenter getPresenter() {
        return _presenter;
    }

    public void setPresenter(MenuPresenter presenter) {
        _presenter = presenter;
    }

    public int getNumberMap() {
        return _numberMap;
    }

    public List<String> getListMap() {
        return _listMap;
    }

    public void displayError(String message) {
        _errorLabel.setText(message);
    }

    @FXML
    private void launchGame() {
        close();
        _presenter.launchGame("coucou");
    }

    @FXML
    public void startLeftShow() {
        _numberMap = _numberMap + 1;
        if (_numberMap == _listMap.size()) {
            _numberMap = 0;
        }
        animation();
    }

    public int getTeamMates() {
        if (this.teamMates.getSelectedToggle().equals(teamMates2)) {
            return 2;
        }
        if (this.teamMates.getSelectedToggle().equals(teamMates4)) {
            return 4;
        }
        return 2;
    }

    public String getSailSize() {
        if (this.sailSize.getSelectedToggle().equals(medium)) {
            return "medium";
        } else {
            return "big";
        }
    }

    @FXML
    public void startRightShow() {
        _numberMap = _numberMap - 1;
        if (_numberMap > _listMap.size() + 1 || _numberMap == -1) {
            _numberMap = _listMap.size() - 1;
        }
        animation();
    }

    public String getImage() {
        if (_listMap.get(_numberMap).equals(MenuView.class.getResource("map_easy.png").toString())) {
            return "SpotMap1.fxml";
        } else {
            return "SpotMap2.fxml";
        }
    }

    @FXML
    private void initialize() {
        teamMates2.setSelected(true);
        medium.setSelected(true);
        _listMap = new ArrayList<>();
        _listMap.add(MenuView.class.getResource("map_easy.png").toString());
        _listMap.add(MenuView.class.getResource("map_hard.png").toString());

        left = new javafx.scene.control.Button("left");
        right = new javafx.scene.control.Button("right");

        images = new javafx.scene.image.Image[_listMap.size()];
        for (int i = 0; i < _listMap.size(); i++) {
            images[i] = new javafx.scene.image.Image(_listMap.get(i));
        }

        imageView.setImage(images[_numberMap]);
    }

    private void animation() {
        FadeTransition ft = new FadeTransition();
        imageView.setImage(images[_numberMap]);
        ft.setDuration(new Duration(500));
        ft.setNode(imageView);
        ft.setFromValue(0.35);
        ft.setToValue(1.0);
        ft.play();
    }

}
