package fr.ensicaen.genielogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.command.ConcreteCommandLeft;
import org.junit.Assert;
import org.junit.Test;

public class ConcreteCommandLeftTest {

    @Test
    public void should_return_minus_one() {
        //when
        ConcreteCommandLeft concreteCommandLeft = new ConcreteCommandLeft();

        //then
        Assert.assertEquals(-1, concreteCommandLeft.execute(), 0);
    }
}
