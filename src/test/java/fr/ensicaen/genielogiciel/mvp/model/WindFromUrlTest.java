package fr.ensicaen.genielogiciel.mvp.model;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class WindFromUrlTest {
    @Test
    public void should_return_windSpeed() {
        //when
        WindFromUrl wfu = new WindFromUrl();
        //then
        assertTrue("Speed Out Of Range", wfu.getWindSpeed()>=0);
    }
}
