package fr.ensicaen.genielogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.command.ConcreteCommandStraight;
import org.junit.Assert;
import org.junit.Test;

public class ConcreteCommandStraightTest {

    @Test
    public void should_return_zero() {
        //when
        ConcreteCommandStraight concreteCommandStraight = new ConcreteCommandStraight();


        //then
        Assert.assertEquals(0, concreteCommandStraight.execute(), 0);
    }

}
