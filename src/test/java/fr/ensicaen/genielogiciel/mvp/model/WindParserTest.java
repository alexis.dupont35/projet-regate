package fr.ensicaen.genielogiciel.mvp.model;

import org.junit.Assert;
import org.junit.Test;

public class WindParserTest {

    @Test
    public void should_return_value_and_angle() {
        //when
        WindParser wp = new WindParser(5, 10);

        //then
        Assert.assertEquals(5, wp.getValue(), 0);
        Assert.assertEquals(10, wp.getAngle(), 0);
    }

    @Test
    public void should_parse_correct_values() {
        //when
        WindParser wp = new WindParser(5, 10);

        //then
        Assert.assertEquals(0.00, wp.getSpeedPolar()[1][2], 0);
        Assert.assertEquals(9.00, wp.getSpeedPolar()[19][14], 0);
    }
}
