package fr.ensicaen.genielogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.command.ConcreteCommandRight;
import org.junit.Assert;
import org.junit.Test;

public class ConcreteCommandRightTest {

    @Test
    public void should_return_one() {
        //when
        ConcreteCommandRight concreteCommandRight = new ConcreteCommandRight();

        //then
        Assert.assertEquals(1, concreteCommandRight.execute(), 0);
    }
}
