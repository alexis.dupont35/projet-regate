package fr.ensicaen.genielogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.decorator.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoatTest {
    @Test
    public void should_set_boat_speed() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));
        WindParser wind = new WindParser(4, 30);
        final double testSpeed = 0.625;

        //when
        wind.initializeSpeedPolar();

        //then
        assertEquals("Not equals", testSpeed, boat.getNewSpeed(wind), 0.0);
    }

    @Test
    public void should_get_boat_angle() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));

        //when
        boat.setAngle(38);

        //then
        assertEquals(38, boat.getAngle(), 0);
    }


    @Test
    public void should_calculate_next_position() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));
        boat.setAngle(90);
        WindParser windParser = new WindParser(10, 90);

        //when
        boat.updatePosition(1, windParser);

        //then
        assertEquals("Not equals", -0.049 ,boat.getX(), 0.001);
        assertEquals("Not equals", 1.87 ,boat.getY(), 0.01);
    }

    @Test
    public void should_set_X() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));

        //when
        boat.setX(1);

        //then
        assertEquals("Not equals", 1.0 ,boat.getX(), 0.0);
    }

    @Test
    public void should_set_Y() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));

        //when
        boat.setY(2);

        //then
        assertEquals("Not equals", 2.0 ,boat.getY(), 0.0);
    }

    @Test
    public void should_set_angle() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));

        //when
        boat.setAngle(90);

        //then
        assertEquals("Not equals", 90 ,boat.getAngle(), 0.0);
    }

    @Test
    public void should_neutralize() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));

        //when
        boat.neutralize();

        //then
        assertTrue("Not True", boat.isNeutralized());
    }

    @Test
    public void should_activate() {
        //given
        Boat boat = new TwoCrewMatesBoatDecorator(new MediumSailedBoatDecorator(new SimpleBoat()));

        //when
        boat.activate();

        //then
        assertFalse("Not Activated", boat.isNeutralized());
    }
}
