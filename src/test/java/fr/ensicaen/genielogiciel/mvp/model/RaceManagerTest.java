package fr.ensicaen.genielogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.decorator.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({BigSailedBoatDecorator.class, RaceManager.class})
public class RaceManagerTest {

    @Mock
    BigSailedBoatDecorator _mockBoat;
    @Mock
    WindFromUrl _mockWindFromUrl;

    @Test
    public void should_return_Boat() throws Exception {
        //given
        PowerMockito.whenNew(BigSailedBoatDecorator.class).withAnyArguments().thenReturn(_mockBoat);
        PowerMockito.whenNew(WindFromUrl.class).withNoArguments().thenReturn(_mockWindFromUrl);
        Mockito.doNothing().when(_mockWindFromUrl).setWindSpeedAndDirection();

        //when
        RaceManager rm = new RaceManager(2, "big");

        //then
        Assert.assertSame(_mockBoat, rm.getBoat());
    }

    @Test
    public void should_return_WindParser_value_and_angle() {
        //when
        WindParser wp = new WindParser(5, 10);

        //then
        Assert.assertEquals(5, wp.getValue(), 0);
        Assert.assertEquals(10, wp.getAngle(), 0);

    }
}
