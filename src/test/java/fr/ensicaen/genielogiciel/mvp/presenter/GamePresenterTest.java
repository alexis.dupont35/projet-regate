package fr.ensicaen.genielogiciel.mvp.presenter;

import fr.ensicaen.genielogiciel.mvp.model.Obstacles;
import fr.ensicaen.genielogiciel.mvp.model.RaceManager;
import fr.ensicaen.genielogiciel.mvp.model.WindFromUrl;
import fr.ensicaen.genielogiciel.mvp.model.decorator.Boat;
import fr.ensicaen.genielogiciel.mvp.view.GameView;
import fr.ensicaen.genielogiciel.mvp.view.MenuView;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.Mockito.times;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GamePresenter.class, GameView.class, MenuPresenter.class, MenuView.class, Obstacles.class, Boat.class, RaceManager.class})
public class GamePresenterTest {

    @Mock
    GameView _mockGameView;
    @Mock
    Obstacles _mockObstacles;
    @Mock
    MenuView _mockMenuView;
    @Mock
    MenuPresenter _mockMenuPresenter;
    @Mock
    WindFromUrl _mockWindFromUrl;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void should_call_game_view_method() throws Exception {
        //given
        Line line = new Line();
        line.setStartX(1.0);
        line.setEndX(2.0);
        line.setStartY(1.0);
        line.setEndY(2.0);

        PowerMockito.whenNew(Obstacles.class).withAnyArguments().thenReturn(_mockObstacles);
        PowerMockito.whenNew(WindFromUrl.class).withNoArguments().thenReturn(_mockWindFromUrl);
        Mockito.doNothing().when(_mockWindFromUrl).setWindSpeedAndDirection();
        Mockito.doNothing().when(_mockGameView).setArrowAngle(ArgumentMatchers.anyDouble());
        Mockito.doReturn(line).when(_mockObstacles).getStartingLine();

        GamePresenter gamePresenter = new GamePresenter(2, "medium", "Map1.txt");
        gamePresenter.setView(_mockGameView);

        //when
        gamePresenter.initializeGame();

        //then
        Mockito.verify(_mockGameView, times(2)).setArrowAngle(Mockito.anyDouble());
    }

    @Test
    public void should_set_presenter_on_view() throws Exception {
        //given
        PowerMockito.mockStatic(MenuView.class);
        PowerMockito.when(MenuView.createView()).thenReturn(_mockMenuView);
        PowerMockito.whenNew(MenuPresenter.class).withNoArguments().thenReturn(_mockMenuPresenter);
        PowerMockito.whenNew(WindFromUrl.class).withNoArguments().thenReturn(_mockWindFromUrl);
        Mockito.doNothing().when(_mockWindFromUrl).setWindSpeedAndDirection();
        Mockito.doCallRealMethod().when(_mockMenuPresenter).setView(ArgumentMatchers.any(MenuView.class));
        Mockito.doCallRealMethod().when(_mockMenuView).setPresenter(ArgumentMatchers.any(MenuPresenter.class));
        Mockito.doNothing().when(_mockMenuPresenter).setView(ArgumentMatchers.any(MenuView.class));
        Mockito.doCallRealMethod().when(_mockMenuView).getPresenter();

        GamePresenter gamePresenter = new GamePresenter(2, "medium", "Map1.txt");

        //when
        gamePresenter.launchMenu();

        //then
        Assert.assertSame(_mockMenuPresenter, _mockMenuView.getPresenter());
    }

    @Test
    public void should_set_view() throws Exception {
        //given
        PowerMockito.whenNew(WindFromUrl.class).withNoArguments().thenReturn(_mockWindFromUrl);
        Mockito.doNothing().when(_mockWindFromUrl).setWindSpeedAndDirection();
        GamePresenter gamePresenter = new GamePresenter(2, "medium", "Map1.txt");

        //when
        gamePresenter.setView(_mockGameView);

        //then
        Assert.assertSame(_mockGameView, gamePresenter.getView());
    }
}
