package fr.ensicaen.genielogiciel.mvp.presenter;

import fr.ensicaen.genielogiciel.mvp.view.GameView;
import fr.ensicaen.genielogiciel.mvp.view.MenuView;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.Mockito.times;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MenuPresenter.class, MenuView.class, GamePresenter.class, GameView.class})
public class MenuPresenterTest {

    @Mock
    MenuPresenter _mockMenuPresenter;
    @Mock
    GamePresenter _mockGamePresenter;
    @Mock
    MenuView _mockMenuView;
    @Mock
    GameView _mockGameView;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void should_call_createAndDisplayGameView() throws Exception {

        // Empty nickname test

        //given
        Mockito.doNothing().when(_mockMenuView).displayError(ArgumentMatchers.anyString());
        Mockito.doCallRealMethod().when(_mockMenuPresenter).launchGame(ArgumentMatchers.anyString());
        Mockito.doCallRealMethod().when(_mockMenuPresenter).setView(ArgumentMatchers.any(MenuView.class));
        _mockMenuPresenter.setView(_mockMenuView);

        //when
        _mockMenuPresenter.launchGame("");

        //then
        Mockito.verify(_mockMenuView, times(1)).displayError(ArgumentMatchers.anyString());

        // Normal test

        //given
        Mockito.doNothing().when(_mockMenuPresenter).createAndDisplayGameView(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
        Mockito.when(_mockMenuPresenter.getCollisionBoxes()).thenReturn("Map1.txt");
        Mockito.when(_mockMenuView.getSailSize()).thenReturn("big");
        Mockito.when(_mockMenuView.getImage()).thenReturn("image");

        //when
        _mockMenuPresenter.launchGame("John");

        //then
        Mockito.verify(_mockMenuPresenter, times(1)).createAndDisplayGameView(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString());

    }

    public void should_set_view() {
        //given
        MenuPresenter menuPresenter = new MenuPresenter();

        //when
        menuPresenter.setView(_mockMenuView);

        //then
        Assert.assertSame(_mockMenuView, menuPresenter.getView());
    }
}
